const ApplicationError = require(`./ApplicationError`);

class RecordNotFoundError extends ApplicationError {
  constructor() {
    super(`Password is not correct!`);
  }
}

module.exports = RecordNotFoundError;
